package de.mipnet.isspass.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.mipnet.isspass.R;

import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class PassesFragment extends Fragment {

    private final String TAG = this.getClass().getSimpleName();

    private PassesViewModel mViewModel;
    private RecyclerView passesRecyclerView;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PassesFragment() {
    }

    public static PassesFragment newInstance() {
        PassesFragment fragment = new PassesFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = new ViewModelProvider(requireActivity()).get(PassesViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.passes_fragment, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            passesRecyclerView = (RecyclerView) view;
            passesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            passesRecyclerView.setAdapter(new PassItemRecyclerViewAdapter(null));
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        final Observer<List<IssPass>> ISSLocationObserver = issPasses -> {
            PassItemRecyclerViewAdapter passItemRecyclerViewAdapter = new PassItemRecyclerViewAdapter(issPasses);
            passesRecyclerView.setAdapter(passItemRecyclerViewAdapter);
        };

        mViewModel.issPasses.observe(this, ISSLocationObserver);
    }
}
