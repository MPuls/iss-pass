package de.mipnet.isspass.ui.main;

import android.util.Log;
import de.mipnet.isspass.apis.n2yo.N2yoVisualPassesResponse;

class IssPass {

    final String TAG = this.getClass().getSimpleName();

    long dateUTC;
    long duration;
    float magnitude;
    float visibility;

    long startUTC;
    String startCompass;
    float startElevation;

    String maxCompass;
    float maxElevation;

    long endUTC;
    String endCompass;
    float endElevation;

    IssPass(N2yoVisualPassesResponse.N2yoPass n2yoPass) {
        this.dateUTC = n2yoPass.startUTC;
        this.duration = n2yoPass.duration;
        this.magnitude = n2yoPass.mag;
        this.visibility = getVisibility(n2yoPass.mag);

        this.startUTC = n2yoPass.startUTC;
        this.startCompass = n2yoPass.startAzCompass;
        this.startElevation = n2yoPass.startEl;

        this.maxCompass = n2yoPass.maxAzCompass;
        this.maxElevation = n2yoPass.maxEl;

        this.endUTC = n2yoPass.endUTC;
        this.endCompass = n2yoPass.endAzCompass;
        this.endElevation = n2yoPass.endEl;
    }

    private float getVisibility(float magnitude) {
        float darkestMag = 1.0f;
        float brightestMag = -2.5f;
        /*
        3.0f -> 0
        -3.0 -> 1

        ( magnitude * -1 + darkestMag ) / (darkestMag - brightestMag)
         */

        float visibility = ( -1 * magnitude + darkestMag ) / ( darkestMag - brightestMag );
        Log.d(TAG, "getVisibility: " + visibility);
        if (visibility < 0) {
            visibility = 0;
        }
        if (visibility > 1) {
            visibility = 1;
        }
        return visibility;
    }
}
