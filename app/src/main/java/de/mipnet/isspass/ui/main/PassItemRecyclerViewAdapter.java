package de.mipnet.isspass.ui.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import de.mipnet.isspass.R;

import java.util.ArrayList;
import java.util.List;

import static de.mipnet.isspass.helper.DateTimeFormatter.*;

public class PassItemRecyclerViewAdapter extends RecyclerView.Adapter<PassItemRecyclerViewAdapter.ViewHolder> {

    private List<IssPass> mValues;

    public PassItemRecyclerViewAdapter(@Nullable List<IssPass> items) {
        if (items == null) {
            mValues = new ArrayList<>();
        } else {
            mValues = items;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.passes_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.vStartDateTime.setText(formatStartDateTime(mValues.get(position).dateUTC));
        holder.vDuration.setText(formatDuration(mValues.get(position).duration));
        holder.vVisibility.setAlpha(mValues.get(position).visibility);

        holder.vStartCompass.setText(mValues.get(position).startCompass);
        holder.vStartElevation.setText(formatElevation(mValues.get(position).startElevation));

        holder.vMaxCompass.setText(mValues.get(position).maxCompass);
        holder.vMaxElevation.setText(formatElevation(mValues.get(position).maxElevation));

        holder.vEndCompass.setText(mValues.get(position).endCompass);
        holder.vEndElevation.setText(formatElevation(mValues.get(position).endElevation));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View vView;

        public final TextView vStartDateTime;
        public final TextView vDuration;
        public final ImageView vVisibility;

        public final TextView vStartCompass;
        public final TextView vStartElevation;

        public final TextView vMaxCompass;
        public final TextView vMaxElevation;

        public final TextView vEndCompass;
        public final TextView vEndElevation;

        public IssPass mItem;

        public ViewHolder(View view) {
            super(view);
            vView = view;

            vStartDateTime = view.findViewById(R.id.start_date_time);
            vDuration = view.findViewById(R.id.duration_value);
            vVisibility = view.findViewById(R.id.visibility);

            vStartCompass = view.findViewById(R.id.startCompass);
            vStartElevation = view.findViewById(R.id.startElevation);

            vMaxCompass = view.findViewById(R.id.maxCompass);
            vMaxElevation = view.findViewById(R.id.maxElevation);

            vEndCompass = view.findViewById(R.id.endCompass);
            vEndElevation = view.findViewById(R.id.endElevation);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + vDuration.getText() + "'";
        }
    }
}
