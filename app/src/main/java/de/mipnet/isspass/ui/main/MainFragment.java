package de.mipnet.isspass.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import de.mipnet.isspass.R;

public class MainFragment extends Fragment {

    private final String TAG = this.getClass().getSimpleName();

    private PassesViewModel mViewModel;
    private TextView vLocationName;
    private ProgressBar vPendingIndicator;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            PassesFragment passesFragment = PassesFragment.newInstance();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.passes, passesFragment);
            transaction.commit();
        }
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = new ViewModelProvider(requireActivity()).get(PassesViewModel.class);
        vLocationName = view.findViewById(R.id.location_name);
        vPendingIndicator = view.findViewById(R.id.pendingIndicator);

        mViewModel.init();
    }

    @Override
    public void onResume() {
        super.onResume();

        final Observer<String> locationNameObserver = name -> vLocationName.setText(name);
        final Observer<Boolean> isPendingObserver = isPending -> vPendingIndicator.setVisibility(isPending ? View.VISIBLE : View.INVISIBLE);
        final Observer<Boolean> showNoPassesPlaceholderObserver = showNoPassesPlaceholder -> {
            if(showNoPassesPlaceholder) {
                NoPassesPlaceholderFragment noPassesPlaceholderFragment = NoPassesPlaceholderFragment.newInstance();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.passes, noPassesPlaceholderFragment);
                transaction.commit();
            }
        };

        mViewModel.locationName.observe(this, locationNameObserver);
        mViewModel.isPending.observe(this, isPendingObserver);
        mViewModel.showNoPassesPlaceholder.observe(this, showNoPassesPlaceholderObserver);
    }
}
