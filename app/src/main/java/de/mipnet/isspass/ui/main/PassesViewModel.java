package de.mipnet.isspass.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import de.mipnet.isspass.apis.n2yo.N2yoVisualPassesRequest;
import de.mipnet.isspass.apis.n2yo.N2yoVisualPassesResponse;
import de.mipnet.isspass.apis.nominatim.NominatimReverseRequest;
import de.mipnet.isspass.apis.nominatim.NominatimReverseResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.mipnet.isspass.PermissionHandler.isGranted;

public class PassesViewModel extends AndroidViewModel {
    private String TAG = this.getClass().getSimpleName();
    @SuppressLint("StaticFieldLeak")
    private Context context;

    RequestQueue requestQueue;

    MutableLiveData<List<IssPass>> issPasses = new MutableLiveData<>();
    MutableLiveData<String> locationName = new MutableLiveData<>();
    MutableLiveData<Boolean> isPending = new MutableLiveData<>();
    MutableLiveData<Boolean> showNoPassesPlaceholder = new MutableLiveData<>();

    private boolean isNomainatimRequestPending;
    private boolean isN2yoRequestPending;


    @SuppressLint("MissingPermission")
    public PassesViewModel(@NonNull Application application) {
        super(application);
        context = application.getApplicationContext();
        requestQueue = Volley.newRequestQueue(context);

        Log.d(TAG, "PassesViewModel: created");
        Log.d(TAG, "PassesViewModel: " + this);

        this.isN2yoRequestPending = true;
        this.isNomainatimRequestPending = true;
        postIsPending();
    }

    public void init() {
        Log.d(TAG, "init: called");

        this.isN2yoRequestPending = true;
        this.isNomainatimRequestPending = true;
        postIsPending();

        performLocationUpdate();
    }

    @SuppressLint("MissingPermission")
    private void performLocationUpdate() {
        Log.d(TAG, "performLocationUpdate: called");
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                onLocationChange(location);
                locationManager.removeUpdates(this);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        if (isGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            Log.d(TAG, "performLocationUpdate: call locationManager.requestLocationUpdates()");
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            Location lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (lastLocation != null) {
                Log.d(TAG, "performLocationUpdate: last known location: " +lastLocation.toString());
                onLocationChange(lastLocation);
            }
        }
    }

    private void onLocationChange(Location location) {
        Log.d(TAG, "onLocationChanged: location: " + location);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        double altitude = location.getAltitude();

        // Munich
                /*
                double longitude = 11.63603;
                double latitude = 48.17027;
                 */

        // Lisboa
               /* double longitude = -9.17;
                double latitude = 38.72;*/

/*        // Rio
        longitude = -43.21;
        latitude = -22.91;
        altitude = 100;*/

/*        // Genava
        longitude = 6.13062;
        latitude = 46.21242;*/

        Log.d(TAG, "location: lon: " + longitude + " | lat: " + latitude + " | alt: " + altitude);

        performN2yoRequestAndPostIssPasses(longitude, latitude, altitude);
        performNomainatimRequestAndPostLocationName(longitude, latitude);
    }

    private void performN2yoRequestAndPostIssPasses(double longitude, double latitude, double altitude) {
        String n2yoUrl = N2yoVisualPassesRequest.getUrl((float) latitude, (float) longitude, Math.round(altitude), 7, 0);
        Log.d(TAG, "n2yoUrl: " + n2yoUrl);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, n2yoUrl, null,
                response -> {
                    Log.d(TAG, "onResponse: response: " + response.toString());

                    Gson gson = new Gson();
                    N2yoVisualPassesResponse issPassResponse = gson.fromJson(response.toString(), N2yoVisualPassesResponse.class);

                    List<IssPass> _issPasses = new ArrayList<>();
                    if (issPassResponse.passes != null && issPassResponse.passes.length > 0) {
                        Arrays.asList(issPassResponse.passes).forEach(pass -> {
                            if (passIsVisible(pass)) {
                                IssPass issPass = new IssPass(pass);
                                _issPasses.add(issPass);
                            }
                        });
                    }
                    if(_issPasses.isEmpty()) {
                        showNoPassesPlaceholder.postValue(true);
                    }

                    issPasses.postValue(_issPasses);

                    this.isN2yoRequestPending = false;
                    postIsPending();
                },
                error -> {
                    Log.e(TAG, "onErrorResponse: " + error.toString(), error);
                    this.isN2yoRequestPending = false;
                    postIsPending();
                }
        );

        requestQueue.add(request);
    }

    private void performNomainatimRequestAndPostLocationName(double longitude, double latitude) {
        String nominatimUrl = NominatimReverseRequest.getUrl(longitude, latitude);
        Log.d(TAG, "nominatimUrl: " + nominatimUrl);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, nominatimUrl, null,
            response -> {
                Log.d(TAG, "onResponse: response: " + response.toString());

                Gson gson = new Gson();
                NominatimReverseResponse nominatimReverseResponse = gson.fromJson(response.toString(), NominatimReverseResponse.class);

                String place = null;
                if (nominatimReverseResponse.address.city != null) {
                    place = nominatimReverseResponse.address.city;
                } else if (nominatimReverseResponse.address.town != null) {
                    place = nominatimReverseResponse.address.town;
                } else if (nominatimReverseResponse.address.village != null) {
                    place = nominatimReverseResponse.address.village;
                }

                if (nominatimReverseResponse.address.country != null && place != null) {
                    place = place + ", " + nominatimReverseResponse.address.country;
                } else if (nominatimReverseResponse.address.country != null) {
                    place = nominatimReverseResponse.address.country;
                } else {
                    place = longitude + ", " + latitude;
                }


                locationName.postValue(place);

                this.isNomainatimRequestPending = false;
                postIsPending();
            },
            error -> {
                Log.e(TAG, "onErrorResponse: ", error);

                this.isNomainatimRequestPending = false;
                postIsPending();
            }
        );
        requestQueue.add(request);
    }

    private boolean passIsVisible(N2yoVisualPassesResponse.N2yoPass pass) {
        return pass.maxEl > 40;
    }

    private void postIsPending() {
        isPending.postValue(isN2yoRequestPending || isNomainatimRequestPending);
    }
}
