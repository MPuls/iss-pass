package de.mipnet.isspass.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import de.mipnet.isspass.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NoPassesPlaceholderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NoPassesPlaceholderFragment extends Fragment {

    public NoPassesPlaceholderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NoPassesPlaceholderFragment.
     */
    public static NoPassesPlaceholderFragment newInstance() {
        NoPassesPlaceholderFragment fragment = new NoPassesPlaceholderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_passes_placeholder, container, false);
    }
}
