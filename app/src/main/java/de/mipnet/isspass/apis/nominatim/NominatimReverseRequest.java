package de.mipnet.isspass.apis.nominatim;

public class NominatimReverseRequest {
    public static String getUrl(double longitude, double latitude) {
        return "https://nominatim.openstreetmap.org/reverse?"
                + "format=json"
                + "&lat=" + latitude
                + "&lon=" + longitude
                + "&addressdetails=10";
    }
}
