package de.mipnet.isspass.apis.nominatim;

public class NominatimReverseResponse {

    public String display_name;
    public NominatimAddress address;

    public static class NominatimAddress {
        public String city;
        public String country;
        public String town;
        public String village;
    }
}
