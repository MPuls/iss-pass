package de.mipnet.isspass.apis.n2yo;

public class N2yoVisualPassesResponse {

    public N2yoInfo info;
    public N2yoPass[] passes;

    public static class N2yoInfo {
        public int satid; // NORAD id
        public String satname; // Satellite name
        public int transactionscount; // Count of transactions performed with this API key in last 60 minutes
        public int passescount; // Count of passes returned
    }

    public static class N2yoPass {
        public float startAz; // Satellite azimuth for the start of this pass (relative to the observer, in degrees)
        public String startAzCompass; // Satellite azimuth for the start of this pass (relative to the observer). Possible values: N, NE, E, SE, S, SW, W, NW
        public float startEl; // Satellite elevation for the start of this pass (relative to the observer, in degrees)
        public int startUTC; // Unix time for the start of this pass. You should convert this UTC value to observer's time zone
        public float maxAz; // Satellite azimuth for the max elevation of this pass (relative to the observer, in degrees)
        public String maxAzCompass; // Satellite azimuth for the max elevation of this pass (relative to the observer). Possible values: N, NE, E, SE, S, SW, W, NW
        public float maxEl; // 	Satellite max elevation for this pass (relative to the observer, in degrees)
        public int maxUTC; // Unix time for the max elevation of this pass. You should convert this UTC value to observer's time zone
        public float endAz; // Satellite azimuth for the end of this pass (relative to the observer, in degrees)
        public String endAzCompass; // Satellite azimuth for the end of this pass (relative to the observer). Possible values: N, NE, E, SE, S, SW, W, NW
        public float endEl; // Satellite elevation for the end of this pass (relative to the observer, in degrees)
        public int endUTC; // Unix time for the end of this pass. You should convert this UTC value to observer's time zone
        public float mag; // Max visual magnitude of the pass, same scale as star brightness. If magnitude cannot be determined, the value is 100000
        public int duration; // Total visible duration of this pass (in seconds)
        public int startVisibility; // Unix time for the start of visibility for this pass. You should convert this UTC value to observer's time zone
    }
}
