package de.mipnet.isspass.apis.n2yo;

import de.mipnet.isspass.BuildConfig;

public class N2yoVisualPassesRequest {

    public static String getUrl(float observer_lat, float observer_lng, float observer_alt, int days, int min_visibility) {
        return "https://api.n2yo.com/rest/v1/satellite/visualpasses/25544/"
                + observer_lat + "/"
                + observer_lng + "/"
                + observer_alt + "/"
                + days + "/"
                + min_visibility + "/"
                + "&apiKey=" + BuildConfig.N2YO_API_KEY;
    }
}
