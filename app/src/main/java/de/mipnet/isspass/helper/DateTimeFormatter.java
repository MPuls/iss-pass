package de.mipnet.isspass.helper;


import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeFormatter {
    private static final String TAG = "DateTimeFormatter";
    private static Locale LOCAL = Locale.getDefault();

    public static String formatDuration(long durationInSec) {
        int minutes = Math.round((float) durationInSec / 60);

        return String.format(LOCAL, "%02d", minutes);
    }

    public static String formatStartDateTime(long unixTimestampInSec) {
        Instant instant = Instant.ofEpochSecond(unixTimestampInSec);
        // ZonedDateTime instantInRightTimeZone = instant.atZone(ZoneId.of("GMT+01:00"));
        ZonedDateTime instantInRightTimeZone = instant.atZone(TimeZone.getDefault().toZoneId());
        return instantInRightTimeZone.format(java.time.format.DateTimeFormatter.ofPattern("E, dd. MMM, HH:mm z", LOCAL));
    }

    public static String toTime(long unixTimestampInSec) {
        Instant instant = Instant.ofEpochSecond(unixTimestampInSec);
        ZonedDateTime instantInRightTimeZone = instant.atZone(TimeZone.getDefault().toZoneId());
        return instantInRightTimeZone.format(java.time.format.DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
    }

    public static String formatMagnitude(float magnitude) {
        return String.format(LOCAL,"%.1f", magnitude);
    }

    public static String formatElevation(float elevation) {
        return String.format(LOCAL,"%.0f", elevation) + "°";
    }

    public static String toLocalTimeZone(long unixTimestampInSec) {
        Instant instant = Instant.ofEpochSecond(unixTimestampInSec);
        // ZonedDateTime instantInRightTimeZone = instant.atZone(ZoneId.of("GMT+01:00"));
        ZonedDateTime instantInRightTimeZone = instant.atZone(TimeZone.getDefault().toZoneId());
        return instantInRightTimeZone.format(java.time.format.DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.SHORT));
    }
}
