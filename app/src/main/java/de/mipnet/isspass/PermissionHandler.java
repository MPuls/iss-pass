package de.mipnet.isspass;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

public class PermissionHandler {

    private static final String TAG = "PermissionHandler";

    public static final int LOCATION_REQUEST_CODE = 0;

    private static final String[] permissionsName = {
        Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    public static boolean isGranted(Context context, String permission) {
        boolean isGranted = ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        Log.d(TAG, permission + " isGranted: " + isGranted);
        return isGranted;
    }

    public static void showLocationPermissionExplanation(Activity activity) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Request Location Permission");
        builder.setMessage("Without granting the access for coarse location to this app, the position to look for the ISS cannot be determined.");

        // add a button
        builder.setPositiveButton("OK", (dialogInterface, i) -> ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE));

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
