# ISS Pass

![ISS Pass Logo](app/src/main/res/mipmap-hdpi/ic_launcher_round.png)

ISS Pass is a [GPL v3](LICENSE) Android App with the main purpose to show the next "visible" transitions of the International Space Station (ISS) over the sky at the user's current location.

It uses the REST-API of [N2YO](https://n2yo.com) to get the passes where the ISS is illuminated by the sun while the sky is still or already dark enough.
Additionally, only passes where the highest elevation is at least 40° are listed.

## UI

![Main screen](doc/ui/main-screen.jpg)

## Build

ISS Pass uses the REST-API of [N2YO](https://n2yo.com). Thus, to use the App, one needs to have a license key from N2YO which is sent as plain text in each API request. The license key is provided after one has created an N2YO account.

In order to not publish a developer's license key the person, who wants to use or develop ISS Pass needs to store his or her own license key in the `./local.properties` under the key of `n2yoApiKey`. The license key is read from there during the build process.
